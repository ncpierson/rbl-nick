# RedBubble Coding Test

Hello 👋

Thanks for looking at my submission.

If you like web interfaces, you can find this project on Gitlab:
[rbl-nick](https://gitlab.com/ncpierson/rbl-nick)

## Getting Started

### 0. Pre-Requisites

You will need NodeJS in order to run the application.
If you do not already have a local installation, I recommend using
[nvm](https://github.com/creationix/nvm).

This codebase was written to support *Node 8*. Specifically, I am on v8.10.0.
Newer versions will likely work.

You will also need [yarn](https://yarnpkg.com/en/docs/install#debian-stable),
though `npm` might work as well.

### 1. Install Dependencies

`$ yarn`

or

`$ npm install`

### 2. Running Tests

To run all tests, including integration tests:

`$ yarn test`

or

`$ npm run test`

### 3. Running the Applicaton

You can use the example files in `integration/examples` as arguments.

`$ node src/index.js integration/examples/cart-4560.json integration/examples/base-prices.json`

The CLI only supports relative pathing (from cwd) at the moment.

## Nothing Is Working!

I packaged the application into executables for linux, mac, and windows using
[pkg](https://github.com/zeit/pkg#readme). This should provide a Plan B in case
you just want to run the app against other examples, but somehow it only runs on
my machine.

Hopefully you have an x64 processor, because I did very little research before
making these executables.

### Linux

`$ ./bin/run-linux integration/examples/cart-4560.json integration/examples/base-prices.json`

### Mac

`$ ./bin/run-macos integration/examples/cart-4560.json integration/examples/base-prices.json`

### Windows

I have no idea how to pass arguments to an exe, sorry 🤷
