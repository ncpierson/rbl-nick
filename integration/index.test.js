const test = require('ava'),
  spawn = require('child_process').spawn,
  concat = require('concat-stream');

function createProcess(appArgs) {
  const args = ['src/index.js'].concat(appArgs);
  return spawn('node', args);
}

function execute(cartPath, pricesPath) {
  const childProcess = createProcess([cartPath, pricesPath]);

  return new Promise(resolve => {
    childProcess.stdout.pipe(concat(result => resolve(result.toString())));
  });
}

test('Cart 4560', async t => {
  const response = await execute(
    'integration/examples/cart-4560.json',
    'integration/examples/base-prices.json'
  );

  t.is(response, '4560\n');
});

test('Cart 9363', async t => {
  const response = await execute(
    'integration/examples/cart-9363.json',
    'integration/examples/base-prices.json'
  );

  t.is(response, '9363\n');
});

test('Cart 9500', async t => {
  const response = await execute(
    'integration/examples/cart-9500.json',
    'integration/examples/base-prices.json'
  );

  t.is(response, '9500\n');
});
