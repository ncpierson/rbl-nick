const test = require('ava'),
  Catalog = require('../src/Catalog');

const hoodie = {
  'product-type': 'hoodie',
  options: {
    colour: ['white', 'black'],
    size: ['small', 'medium']
  },
  'base-price': 3800
};

const hoodieLarge = {
  'product-type': 'hoodie',
  options: {
    colour: ['white', 'black'],
    size: ['large']
  },
  'base-price': 4000
};

const sticker = {
  'product-type': 'sticker',
  options: {},
  'base-price': 200
};

test('Catalog find variant returns 0 if product not found', t => {
  const catalog = new Catalog([]);

  const variant = {
    'product-type': 'hoodie',
    options: {}
  };

  const price = catalog.find(variant);

  t.is(price, 0);
});

test('Catalog find variant returns 0 if variant not found', t => {
  const variant = {
    'product-type': 'hoodie',
    options: { colour: 'red', size: 'small' }
  };

  const catalog = new Catalog([hoodie]);

  const price = catalog.find(variant);

  t.is(price, 0);
});

test('Catalog find variant returns price if found', t => {
  const variant = {
    'product-type': 'hoodie',
    options: {
      colour: 'white',
      size: 'small'
    }
  };

  const catalog = new Catalog([hoodie]);

  const price = catalog.find(variant);

  t.is(price, 3800);
});

test('Catalog find variant can search multiple products', t => {
  const variant = {
    'product-type': 'sticker',
    options: {}
  };

  const catalog = new Catalog([hoodie, sticker]);

  const price = catalog.find(variant);

  t.is(price, 200);
});

test('Catalog find variant can search options', t => {
  const variant = {
    'product-type': 'hoodie',
    options: { colour: 'white', size: 'large' }
  };

  const catalog = new Catalog([hoodie, hoodieLarge]);

  const price = catalog.find(variant);

  t.is(price, 4000);
});

test('Catalog find variant can ignore options that do not affect price', t => {
  const variant = {
    'product-type': 'hoodie',
    options: { colour: 'white', size: 'small', gift: 'yes' }
  };

  const catalog = new Catalog([hoodie]);

  const price = catalog.find(variant);

  t.is(price, 3800);
});

test('Catalog find variant supports base price options in any order', t => {
  const priceWithUnorderedOptions = {
    ...hoodie,
    options: {
      size: ['small'],
      colour: ['white']
    }
  };

  const variant = {
    'product-type': 'hoodie',
    options: { size: 'small', colour: 'white' }
  };

  const catalog = new Catalog([priceWithUnorderedOptions]);

  const price = catalog.find(variant);

  t.is(price, 3800);
});
