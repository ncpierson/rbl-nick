const test = require('ava'),
  sinon = require('sinon'),
  Store = require('../src/Store');

const hoodie = {
  'product-type': 'hoodie',
  options: [],
  'base-price': 3800,
  'artist-markup': 0,
  quantity: 1
};

const sticker = {
  'product-type': 'sticker',
  options: [],
  'base-price': 200,
  'artist-markup': 0,
  quantity: 1
};

test('Store uses catalog during checkout', t => {
  const catalog = { find: sinon.fake() };
  const cart = [hoodie];

  const store = new Store(catalog);

  store.checkout(cart);

  t.truthy(catalog.find.calledOnce);
  t.truthy(catalog.find.calledWith(sinon.match.has('product-type', 'hoodie')));
  t.truthy(catalog.find.calledWith(sinon.match.has('options', [])));
  t.truthy(catalog.find.calledWith(sinon.match.has('base-price', 3800)));
});

test('Store uses catalog prices to compute total', t => {
  const catalog = { find: sinon.stub() };
  catalog.find.onCall(0).returns(3800);
  catalog.find.onCall(1).returns(200);

  const cart = [hoodie, sticker];

  const store = new Store(catalog);

  const price = store.checkout(cart);

  t.is(price, 3800 + 200);
});

test('Store respects item quantity', t => {
  const catalog = { find: sinon.fake.returns(3800) };

  const twoHoodies = { ...hoodie, quantity: 2 };
  const cart = [twoHoodies];

  const store = new Store(catalog);

  const price = store.checkout(cart);

  t.is(price, 3800 * 2);
});

test('Store respects artist markup', t => {
  const catalog = { find: sinon.fake.returns(3800) };

  const hoodieWithMarkup = { ...hoodie, 'artist-markup': 20 };
  const cart = [hoodieWithMarkup];

  const store = new Store(catalog);

  const price = store.checkout(cart);

  t.is(price, 3800 * 1.2);
});

test('Store rounds to nearest cent after markup', t => {
  const catalog = { find: sinon.fake.returns(25) };

  const hoodieWithMarkup = { ...hoodie, 'artist-markup': 10 };
  const cart = [hoodieWithMarkup];

  const store = new Store(catalog);

  const price = store.checkout(cart);

  t.is(price, Math.round(25 * 1.1));
});
