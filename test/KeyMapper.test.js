const test = require('ava'),
  { fromPriceToKeys, fromVariantToKey } = require('../src/KeyMapper');

const priceHoodieSmallWhite = {
  'product-type': 'hoodie',
  options: {
    colour: ['white'],
    size: ['small']
  }
};

test('KeyMapper from price maps product name', t => {
  const priceLeggings = {
    'product-type': 'leggings',
    options: {},
    'base-price': 100
  };

  const keys = fromPriceToKeys(priceLeggings);

  t.deepEqual(keys, ['leggings']);
});

test('KeyMapper from price maps options', t => {
  const priceStickerSmall = {
    'product-type': 'sticker',
    options: {
      size: ['small']
    }
  };

  const keys = fromPriceToKeys(priceStickerSmall);

  t.deepEqual(keys, ['sticker:small']);
});

test('KeyMapper from price handles multiple option types', t => {
  const keys = fromPriceToKeys(priceHoodieSmallWhite);

  t.deepEqual(keys, ['hoodie:white,small']);
});

test('KeyMapper from price handles multiple options', t => {
  const priceHoodieMultipleOptions = {
    ...priceHoodieSmallWhite,
    options: {
      colour: ['white', 'black'],
      size: ['small', 'medium']
    }
  };

  const keys = fromPriceToKeys(priceHoodieMultipleOptions);

  t.deepEqual(keys.sort(), [
    'hoodie:black,medium',
    'hoodie:black,small',
    'hoodie:white,medium',
    'hoodie:white,small'
  ]);
});

test('KeyMapper from price normalizes option order', t => {
  const priceWithUnorderedOptions = {
    ...priceHoodieSmallWhite,
    options: {
      size: ['small'],
      colour: ['white']
    }
  };

  const keys = fromPriceToKeys(priceWithUnorderedOptions);

  t.deepEqual(keys, ['hoodie:white,small']);
});

test('KeyMapper from variant maps product name', t => {
  const variantLeggings = {
    'product-type': 'leggings',
    options: {}
  };

  const key = fromVariantToKey(variantLeggings, []);

  t.is(key, 'leggings');
});

test('KeyMapper from variant maps options', t => {
  const variantHoodie = {
    'product-type': 'hoodie',
    options: {
      colour: 'white',
      size: 'small'
    }
  };

  const key = fromVariantToKey(variantHoodie, ['colour', 'size']);

  t.is(key, 'hoodie:white,small');
});

test('KeyMapper from variant maps only required options', t => {
  const variantHoodie = {
    'product-type': 'hoodie',
    options: {
      colour: 'white',
      size: 'small'
    }
  };

  const key = fromVariantToKey(variantHoodie, ['colour']);

  t.is(key, 'hoodie:white');
});
