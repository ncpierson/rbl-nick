const fs = require('fs'),
  Catalog = require('./Catalog'),
  Store = require('./Store');

function readFile(path) {
  return new Promise((resolve, reject) => {
    fs.readFile(
      path,
      'utf8',
      (err, data) => (err ? reject(err) : resolve(data))
    );
  });
}

const args = process.argv.slice(2);

const cartPromise = readFile(args[0]);
const basePricesPromise = readFile(args[1]);

Promise.all([cartPromise, basePricesPromise])
  .then(strings => strings.map(JSON.parse))
  .then(([cart, basePrices]) => {
    const catalog = new Catalog(basePrices);
    const store = new Store(catalog);

    console.log(store.checkout(cart)); // eslint-disable-line
  });
