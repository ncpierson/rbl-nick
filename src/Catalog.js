const { fromPriceToKeys, fromVariantToKey } = require('./KeyMapper');

class Catalog {
  constructor(basePrices) {
    this.productOptions = createProductOptionsMap(basePrices);
    this.prices = createPriceMap(basePrices);
  }

  find(variant) {
    const variantOptions = this.productOptions.get(variant['product-type']);

    if (variantOptions === undefined) return 0;

    const variantKey = fromVariantToKey(variant, variantOptions);

    return this.prices.get(variantKey) || 0;
  }
}

function createPriceMap(basePrices) {
  return basePrices.reduce((map, basePrice) => {
    const keys = fromPriceToKeys(basePrice);
    const price = basePrice['base-price'];

    return keys.reduce((map, key) => map.set(key, price), map);
  }, new Map());
}

function createProductOptionsMap(basePrices) {
  return basePrices.reduce((map, basePrice) => {
    const product = basePrice['product-type'];

    if (map.has(product)) return map;

    const options = Object.keys(basePrice.options).sort();

    return map.set(product, options);
  }, new Map());
}

module.exports = Catalog;
