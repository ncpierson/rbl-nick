function fromPriceToKeys({ 'product-type': productName, options }) {
  const optionNames = Object.keys(options).sort();

  if (optionNames.length === 0) return [productName];

  const optionValues = optionNames.map(name => options[name]);
  const optionCombinations = combinations(optionValues);

  return optionCombinations.map(combination => {
    const optionsKey = combination.join(',');
    return productName + ':' + optionsKey;
  });
}

function fromVariantToKey(
  { 'product-type': productName, options },
  optionNames
) {
  const optionValues = optionNames.map(name => options[name]);

  const optionsKey = optionValues.join(',');

  return optionsKey.length === 0 ? productName : `${productName}:${optionsKey}`;
}

function combinations([heads, ...rest]) {
  if (heads === undefined) return [];
  if (rest.length === 0) return heads.map(head => [head]);

  const tails = combinations(rest);

  return heads.reduce((res, head) => {
    const combined = tails.map(tail => [head, ...tail]);
    return res.concat(combined);
  }, []);
}

module.exports = { fromPriceToKeys, fromVariantToKey };
