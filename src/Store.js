const fromPercentToDecimal = percent => percent / 100;

function computeMarkup(price, percent) {
  const markupMultiplier = fromPercentToDecimal(percent);

  return Math.round(price * markupMultiplier);
}

class Store {
  constructor(catalog) {
    this.catalog = catalog;
  }

  getLineItemTotal(lineItem) {
    const basePrice = this.catalog.find(lineItem);
    const markup = computeMarkup(basePrice, lineItem['artist-markup']);
    const priceWithMarkup = basePrice + markup;

    return priceWithMarkup * lineItem.quantity;
  }

  checkout(cart) {
    return cart.reduce(
      (total, lineItem) => total + this.getLineItemTotal(lineItem),
      0
    );
  }
}

module.exports = Store;
